#!/usr/bin/env python
import random
import sys
import time

import pika

if __name__ == "__main__":
    idx = int(sys.argv[1])
    host = sys.argv[2]
    user = sys.argv[3]
    password = sys.argv[4]

    username = user + "_" + str(idx)

    # getting a connection to the broker
    credentials = pika.PlainCredentials(username=username, password=password)
    parameters = pika.ConnectionParameters(host, 5672, credentials=credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    # declaring the queue
    channel.queue_declare(queue="fault_injection")
    # send the message, through the exchange ''
    # which simply delivers to the queue having the key as name
    while 1:
        number = random.randint(0, 100)
        channel.basic_publish(
            exchange="", routing_key="fault_injection", body=str(idx) + ";" + str(number)
        )

        with open(f"/tmp/rabbitmq/producer_{idx}_output.txt", "a") as f:
            f.write(" [x] Producer " + str(idx) + " has sent " + str(number) + "\n")

        time.sleep(1.0)

    # gently close (flush)
    connection.close()
