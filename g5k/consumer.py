#!/usr/bin/env python
import sys
import time

import pika


# defining what to do when a message is received
def callback(ch, method, properties, body):
    body_str = body.decode("utf8").replace("'", '"')
    id_sender, number = body_str.split(";")

    with open(f"/tmp/rabbitmq/consumer_{idx}_output.txt", "a") as f:  # append mode
        f.write(
            " [x] Consumer "
            + str(idx)
            + " has received "
            + str(number)
            + " from "
            + str(id_sender)
            + "\n"
        )

    time.sleep(1.0)

    # Manually acknowledge the message
    ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
    global idx

    idx = int(sys.argv[1])
    host = sys.argv[2]
    user = sys.argv[3]
    password = sys.argv[4]

    username = user + "_" + str(idx)

    # getting a connection to the broker
    credentials = pika.PlainCredentials(username=username, password=password)
    parameters = pika.ConnectionParameters(host, 5672, credentials=credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    # declaring the queue again (to be sure)
    channel.queue_declare(queue="fault_injection")

    # auto_ack: as soon as collected, a message is considered as acked
    channel.basic_consume(queue="fault_injection", auto_ack=False, on_message_callback=callback)

    # wait for messages
    channel.start_consuming()
